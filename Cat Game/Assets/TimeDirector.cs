﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeDirector : MonoBehaviour
{
    GameObject timetext;
    float time = 60.0f;

    // Start is called before the first frame update
    void Start()
    {
        this.timetext = GameObject.Find("Time");
    }

    // Update is called once per frame
    void Update()
    {
        this.time -= Time.deltaTime;
        this.timetext.GetComponent<Text>().text = this.time.ToString("F1");
    }
}

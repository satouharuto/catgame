﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedArrowController : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("player"); //追加 
    }

    // Update is called once per frame
    void Update()
    {
        //フレームごとに等速で横に移動
        transform.Translate(-0.1f, 0, 0);

        //画面外に出たら削除
        if (transform.position.x < -10.0f)
        {
            Destroy(gameObject);
        }

        //あたり判定
        Vector2 p1 = transform.position;  //矢の中心座標
        Vector2 p2 = this.player.transform.position; //プレイヤーの中心座標
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f; //矢の半径
        float r2 = 1.0f; //プレイヤーの半径

        if (d < r1 + r2)
        {
           // GameObject director = GameObject.Find("hpGaugeDirector");
           // director.GetComponent<hpGaugeDirector>().DecreaseHp();

            //衝突した場合矢を消す
            Destroy(gameObject);
        }
    }
}
